 #! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Programa que rep 1 argument i diu quin tipus és
# -----------------------------------------

ERR_NARGS=1
ERR_DIR=2

# Validar arguments

if [ $# -ne 1 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Codi

fitxer=$1

if [ ! -e $fitxer ];then
  echo "Error: $fitxer no existeix."
  exit $ERR_DIR
fi

if [ -h $fitxer ];then
  echo "$fitxer és un link."
elif [ -f $fitxer ];then
  echo "$fitxer és un regular file."
elif [ -d $fitxer ];then
  echo "$fitxer és un directori."
else
  echo "$fitxer no és regular file, directori o link."
fi
exit 0

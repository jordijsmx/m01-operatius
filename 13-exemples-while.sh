#| /bin/bash
# @jordijsmx ASIX-M01
# Març 2023
#
# SINOPSIS
# Exemple bucle while
# ------------------------i

# Numera i mostra en majúscules stdin
num=1
while read -r line
do
  maj=$(tr 'a-z' 'A-Z')
  echo "$num: $line" | $maj
  ((num++))
done
exit 0

# Processar stdin linia a linia fins token FI
read -r line                # Hem d'inicialitzar line abans per poder fer la condició després
while [ $line != "FI" ]
do
  echo $line
  read -r line              # Per tornar a demanar stdin
done
exit 0

# Processar stdin linia a linia numerada
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# Processar stdin linia a linia

while read -r line    # També se li pot passar stdin amb "prog < prog o ordre | prog"
do
  echo "$line"        # Si li passem un fitxer o ordre, ja sap quina és l'ultima línia
done
exit 0

# Iterar per la llista d'arguments
while [ -n "$1" ]  # Si $1 existeix, fa el bucle
do
  echo "$1 $# $*"
  shift             # Desplaça els arguments una posició endavant. El que es desplaça es perd
done
exit 0

# Comptador decreixent desde n(arg) fins a 0
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo "$num"
  ((num--))
done
exit 0

# Mostrar un comptador del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0

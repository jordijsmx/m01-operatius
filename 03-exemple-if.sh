#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Exemple if:
#   indicar si ets major d'edat.
#   $ prog edat (sinopsis) 
# -------------------------

# 1) Validar arguments
if [ $# -ne 1 ]; then # Ha d'haver-hi espai als brackets
  echo "Error: número d'arguments incorrecte." 
  echo "Usage: $0 edat"
  exit 1
fi

# 2) Codi
edat=$1 # noms de variables significatius, no val nom1, nom2...

# llista_noms, llista_Noms
# Mai començen amb majúscules
# CONSTANTS en majúscules

if [ $edat -lt 18 ]; then
  echo "Tens $edat anys, ets menor d'edat." # Ha de tenir cometes dobles
elif [ $edat -lt 65 ]; then
  echo "Tens $edat anys, ets població activa."
else
  echo "Tens $edat anys, ets jubilat."
fi

exit 0

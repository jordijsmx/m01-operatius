# @jordijsmx ASIX-M01
# Març 2023
#
# Rep una o més notes d'un alumne i indica, per cada nota rebuda
# si és un suspès, aprovat, notable o excel·lent.
# ------------------------

ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 nota..."
  exit $ERR_NARGS
fi

# Validar que els arguments són un nombre [0-10]
# i codi
for nota in $*
do

  if ! [ $nota -ge 0 -a $nota -le 10 ];then
    echo "Error: Nota $nota incorrecta. [0-10]" >> /dev/stderr # append per si hi ha més d'un error
  elif [ $nota -lt 5 ];then
    echo "Has tret un $nota. Suspès."
  elif [ $nota -lt 7 ];then
    echo "Has tret un $nota. Aprovat."
  elif [ $nota -lt 9 ];then
    echo "Has tret un $nota. Notable."
  else
    echo "Has tret un $nota. Excel·lent."
  fi

done
exit 0	

#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Exercicis test
# -----------------------------------------

# 1. file (element) not exists

test ! -e $element

# 2. is not a regular file

test ! -f $element

# 3. element in the filesystem exists

test -e $element

# 4. element is readable

test -r $element

# 5. num is not equal to 10

test $num -ne 10

# 6. num is less or equal to 100

test $num -le 100

# 7. name is not pere, not marta, not anna

test $name != pere -a $name != marta -a $name != anna 

# 8. num is 10 or 15 or 20

test $num -eq 10 -o $num -eq 15 -o $num -eq 20

# 9. num in the ranges [0,18], [65,120]

test $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120

# 10. num is not in the range [25,50[

test ! $num -ge 25 -a $num -lt 50 # seguir literalment l'enunciat

[ $num -lt 25 -o $num -ge 50 ] # no correcte, però el contrari natural

# 11. error if argos not: arg1 arg2 arg3

[ $# -ne 3 ]

# 12. error if argos not: arg1...

! [ $# -ge 1 ]  |  [ $# -eq 0  ]

# 13. error if argos not: arg1 arg2...

! [ $# -ge 2 ]  |  [ $# -lt 2  ]

# 14. var name has no value, is empty

[ -z "$name" ]

# 15. var num is not null

[ -n $num ]


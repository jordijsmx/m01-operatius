#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Exemple de processar arguments
# -------------------------------

echo '$*: ' $* # Llista d'arguments
echo '$@: ' $@ # Llista d'arguments
echo '$#: ' $# # Num d'arguments
echo '$0: ' $0 # nom del programa
echo '$1: ' $1 # primer argument
echo '$2: ' $2 # ...
echo '$9: ' $9
echo '$10: '${10} # 2 digits han d'anar encapsulats 
echo '$11: '${11}
echo '$$: ' $$ # PID
nom="puig"
echo "${nom}deworld" # si es genera confusió, utilitzar claudàtors 

#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# prog dir
# Validar que es rep un argument i que és un directori i llistar-ne el contingut.
# --------------------
ERR_NARGS=1
ERR_NODIR=2

# Validar que es rep un argument
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Codi
for dir in $*
do
  if ! [ -d $dir ];then
    echo -e "\nError: $dir no és un directori." >> /dev/stderr # o 1>&2
  else
    echo -e "\nMostrant $dir"
    llistat=$(ls $dir)
    for elem in $llistat
    do
      if [ -h "$dir/$elem" ];then
        echo -e "\t$elem és un link."
      elif [ -f "$dir/$elem" ];then
        echo -e "\t$elem és un regular file."
      elif [ -d "$dir/$elem" ];then
        echo -e "\t$elem és un directori."
      else
        echo -e "\t$elem és una altra cosa."
      fi
    done
  fi
done
exit 0

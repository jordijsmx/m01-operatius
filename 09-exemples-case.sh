#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Exemples de case
# ------------------------

case $1 in
   d[ltcjv]) 
   echo "Dia laborable."
    ;;
   d[sm])
    echo "Festiu"
    ;;
   *)
    echo "Altre"
esac
exit 0

case $1 in
  [aeiou])
   echo "És una vocal."
   ;;
  [bcdfghjklmnpqrstvwxyz])
   echo "És consonant."
   ;;
  *)
   echo "És una altra cosa."
esac
exit 0

case $1 in
  "pere"|"marta"|"joan")
    echo "És un nen."
    ;;
  "marta"|"anna"|"julia")
    echo "És una nena."
    ;;
  *)
    echo "No binari."
    ;;
esac
exit 0


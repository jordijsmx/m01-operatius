#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Exemples bucles
# ---------------------

# Llistar tots els logins ordenats i numerats

llista_logins=$(cut -d: -f1 /etc/passwd | sort)
num=1

for login in $llista_logins
do
  echo $num: $login
  ((num++))
done
exit 0

# Llista els fitxers del directori actiu, numerats

llistat=$(ls)
num=1

for nom in $llistat 
do
  echo "$num: $nom"
  ((num++))  
done
exit 0

# Numerar els arguments

num=1 # la inicialització va FORA del bucle, en funció de com l'inicialitzem, l'increment ha d'anar abans o després.

for arg in $*
do
  echo "$num: $arg"
  num=$((num+1)) # l'increment ha d'anar a DINS del bucle
done
exit 0

# Iterar i mostrar la llista d'arguments

for arg in $*  # també pot ser "$@", # encapsular " ". Itera UNA sola vegada
do
  echo "$arg"
done
exit 0

# Iterar pel valor d'una variable

llistat=$(ls)

for nom in $llistat # obtè llista de valors
do
  echo "$nom"       # realitza l'acció a la llista
done
exit 0

# Iterar per un conjunt d'elements

for nom in "pere marta pau anna"
do
  echo "$nom"
done
exit 0

# Iterar per un conjunt d'elements

for nom in "pere" "marta" "pau" "anna" # processa els elements de la llista
do
  echo "$nom"
done
exit 0

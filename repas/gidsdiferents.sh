#!/bin/bash


tots_gids=$(cut -d: -f4 $1)
llista_gids=$(cut -d: -f4 $1 | sort -nu)
file=$1
while read -r line
do
for gid in $llista_gids
do
  num=$(echo "$tots_gids" | grep -cw "^$gid$")
  echo "$gid:$num"
done
done < $file
exit 0

#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# prog dir
# Validar que es rep un argument i que és un directori i llistar-ne el contingut.
# --------------------
ERR_NARGS=1
ERR_NODIR=2

# Validar que es rep un argument
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Validar que és un directori
dir=$1
if ! [ -d $dir ];then
  echo "Error: $dir no és un directori."
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# Codi
ls -l $dir
exit 0

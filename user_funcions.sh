#! /bin/bash
#
# demana un argument i mostra els camps de comptes d'usuaris.
# Si executem el prog amb bash, les funcions es carreguen a la memòria pero no s'executen
# L'hem d'executar amb source o " . ". Podem veure que carrega les funcions amb l'ordre set.
# Per executar les funcions, cridem a les funcions directament a la sessió actual.
# $ showUid 1001
# $ suma 2 3
# --------------------------
function showUid(){
  uid=$1
  linia=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  login=$(echo $linia | cut -d: -f1)
  gid=$(echo $linia | cut -d: -f4)
  gecos=$(echo $linia | cut -d: -f5)
  home=$(echo $linia | cut -d: -f6)
  shell=$(echo $linia | cut -d: -f7)

echo "login: $login"
echo "uid: $1"
echo "gid: $gid"
echo "gecos: $gecos"
echo "home: $home"
echo "shell: $shell"
return 0
}

function suma(){
  suma=$(($1+$2))
  echo $suma
  exit 0   # si posem exit, tanca la sessió actual
}

function multiplica(){
  multiplica=$(($1*$2))
  echo $multiplica
  return 0  # equivalent a exit 0, però no tanca la sessió
}

function showLlistaUids(){
  # Processa la llista de uids, cridant a la funció showUid. Per cada UID valida si existeix. Si existeix, cridem a la funció
  # Si no existeix, mostrem error per /dev/stderr
  for uid in $*
  do
    grep "^[^:]*:[^:]*:$uid:" /etc/passwd > /dev/null
   if [ $? -eq 0 ];then
     showUid $uid
     echo "-----------------------"
   else
    echo "Error: UID $arg no existeix." >> /dev/stderr
   fi 
  done
  return 0
}

function informeShell(){
  # Genera un informe. Per cada shell d'usuari, ens mostra les linies dels usuaris que el fan server.
  llista_shell=$(cut -d: -f7 < /etc/passwd | sort -u)
  
  for shell in $llista_shell
  do
    numLin=$(grep -c ":$shell$" /etc/passwd)
    if [ $numLin -ge 3 ];then
    echo "Shell: $shell --------"
    grep ":$shell$" /etc/passwd | sed -r 's/^(.*)$/\t\1/'
    fi
  done
}


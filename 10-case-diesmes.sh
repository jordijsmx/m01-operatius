#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Dies del mes
# ---------------------

ERR_NARGS=1
ERR_MES=2

# Validar arguments

if [ $# -ne 1 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

# Validar mesos

mes=$1

if ! [ $mes -ge 1 -a $mes -le 12 ];then
  echo "Error: número de mes incorrecte [1-12]"
  exit $ERR_MES
fi

# Codi

case $mes in
  "2")
  dies=28
  ;;
  "4"|"6"|"9"|"11")
  dies=30
  ;;
  *)
  dies=31
  ;;
esac
echo "El mes $mes té $dies dies."
exit 0
  

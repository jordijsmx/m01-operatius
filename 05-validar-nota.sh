#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Programa que rep 1 nota i diu si està aprovat o no
# -----------------------------------------

# Validar número d'arguments

ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# Validar la nota

if ! [ $1 -ge 0 -a $1 -le 10 ];then
  echo "Error: Introdueix una nota entre 1 i 10."
  echo "Usage: $0 nota."
  exit $ERR_NOTA
fi

# Codi
nota=$1
if [ $1 -ge 5 ];then
  echo "La teva nota és $nota, has aprovat."
else
  echo "La teva nota és $nota, has suspès."
fi
exit 0

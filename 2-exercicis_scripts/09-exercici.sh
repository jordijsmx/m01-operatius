#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 09-exercici.sh
# Programa: prog [-r -m -c cognom -j -e edat] arg...
# Desa en les variables "opcions", "cognom", "edat" i "arguments" els valors corresponents.
# -----------------------------------------------------------------------------------------

opcions=
cognom=
edat=
arguments=

while [ -n "$1" ]
do
  case $1 in
    "-r"|"-m"|"-j")
     opcions="$opcions $1";;
   "-c")
     shift
     cognom=$1;;
   "-e") 
     shift
     edat=$1;;
    *)
     arguments="$arguments $1";;
  esac
shift
done
echo "Opcions:$opcions"
echo "Cognom: $cognom"
echo "Edat: $edat"
echo "Arguments:$arguments"
exit 0

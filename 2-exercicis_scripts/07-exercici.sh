#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 07-exercici.sh
# Programa: prog -f|-d arg1 arg2 arg3 arg4
# - Valida que els quatre arguments rebuts són tots els tipus que indica el flag. Si es
#   crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre
#   són directoris.
# - Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
# - Exemple: prog -f carta.txt a.txt /tmp fi.txt --> retorna status 2
# - Ampliar amb el cas: prog -h|--help
# -----------------------------

status=0
ERR_NARGS=1

# Validar arguments
if [ $# -ne 5 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARGS
fi

if [ $1 != "-f" -a $1 != "-d" ];then
  echo "Error: Format d'arguments incorrecte."
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARGS
fi

# Codi
opcio=$1
shift
for arg in $* 
do
  if ! [ $opcio $arg ];then
    status=2
  fi
done
exit $status

#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 12-exercici.sh
# Programa -h uid...
# Per a cada uid mostra la informació de l'usuari en format:
# login(uid) gname home shell
# -------------------------------------------------------

ERR_NARGS=1

# Validar argument
if [ $1 != "-h" ];then
  echo "Error: argument incorrecte."
  echo "Usage: $0 -h uid..."
  exit $ERR_NARGS
fi

shift
for arg in $*
do
  grep  "^[^:]*:[^:]*:$arg:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ];then
    login=$(grep  "^[^:]*:[^:]*:$arg:" /etc/passwd | cut -d: -f1,3 | sed -r 's/^(.*):(.*)/\1(\2)/')
    gid=$(grep  "^[^:]*:[^:]*:$arg:" /etc/passwd | cut -d: -f4)
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    homeshell=$(grep  "^[^:]*:[^:]*:$arg:" /etc/passwd | cut -d: -f6,7 | tr ':' ' ')
    echo "$login $gname $homeshell"
  else
    echo "Error: UID $arg no existeix." >> /dev/stderr
  fi
done
exit 0

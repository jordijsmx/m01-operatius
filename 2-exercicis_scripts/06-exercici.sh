#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 06-exercici.sh
# Processa per stdin línies d'entrada tipus "Tom Snyder" i mostra per stdout la línia
# en format "T. Snyder".
# ---------------------------------------------

while read -r line
do
  nom=$(echo "$line" | cut -c1)
  cognom=$(echo "$line" | cut -d' ' -f2)
  echo "$nom.$cognom"
done
exit 0

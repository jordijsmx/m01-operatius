
#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 04-exercici.sh
# Processa stdin mostrant per stdout les línies numerades i en majúscules
# ---------------------------------------------

num=1

while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

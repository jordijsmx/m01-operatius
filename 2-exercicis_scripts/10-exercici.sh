#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 10-exercici.sh
# Rep per stdin GIDs i llista per stdout la informació de cada un d'aquests grups
# en format: gname: GNAME, gid: GID, users: USERS
# -------------------------------------------------

while read -r gid
do
  grup=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ];then
    nom=$(echo "$grup" | cut -d: -f1)
    users=$(echo "$grup" | cut -d: -f4)
    echo "gname: $nom, gid: $gid, users: $users"
  else
    echo "Error: $gid no existeix." >> /dev/stderr
  fi
done
exit 0

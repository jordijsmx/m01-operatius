
#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 03-exercici.sh
# Processa arguments que són matricules:
#     - Valides: 9999-AAA
#     - stdout les vàlides, stderr les no vàlides, retorna de status
#	el número d'errors de no vàlides.
# ---------------------------------------------
num=0

for matricula in $*
do
  echo "$matricula" | grep -E "^[0-9]{4}-[A-Z]{3}$" &> /dev/null
  if [ $? -eq 0 ];then
    echo "$matricula"
  else
    echo "$matricula" >> /dev/stderr	  
    ((num++))
  fi
done
exit $num

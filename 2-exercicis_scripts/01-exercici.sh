#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 01-exercici.sh
# Processa els arguments i mostra per stdout nomes els de 4 o més caràcters
# ---------------------------------------

for char in $*
do
  linia=$(echo -n "$char" | wc -c)
  if [ $linia -ge 4 ];then
    echo $char
  fi
done
exit 0

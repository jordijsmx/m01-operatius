
#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 05-exercici.sh
# Processa stdin mostrant per stdout les línies de menys de 50 caràcters
# ---------------------------------------------

while read -r line
do
  comptar=$(echo "$line" | wc -c)
  if [ $comptar -lt 50 ];then
  echo $line
  fi
done
exit 0

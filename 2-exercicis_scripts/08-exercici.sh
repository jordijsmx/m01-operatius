#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 08-exercici.sh
# Programa: prog file...
# - Validar que existeix almenys un file.
# - Per cada file, comprimir-lo.
#   - Si s'ha comprimit correctament, missatge per stdout
#   - Si no, missatge error per stderr
# - Mostra per stdout quants files s'han comprimit.
# Retorna status 0 ok, 1 error nº args, 2 error en comprimir.
# ----------------------------------------------------------

status=0
ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 file..."
  exit $ERR_NARGS
fi

# Codi
num=0

for arg in $*
do
  gzip $arg 2> /dev/null
  if [ $? -eq 0 ];then
    echo "$arg"
    ((num++))
  else
    echo "Error: $arg no s'ha comprimit correctament." >> /dev/stderr
    status=2
  fi
done
echo "S'han comprimit $num fitxers correctament."
exit $status

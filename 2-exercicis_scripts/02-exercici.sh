#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 02-exercici.sh
# Processa els arguments i compta quante n'hi ha de 3 o més charàcters
# -----------------------------

num=0

for arg in $*
do
  comptar=$(echo "$arg" | wc -c)
  if [ $comptar -ge 3 ];then
    ((num++))
  fi
done
echo "Hi ha $num argument/s de 3 o més caràcters."
exit 0

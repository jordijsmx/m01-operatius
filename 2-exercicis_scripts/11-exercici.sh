#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 11-exercici.sh
# Rep com a arguments GIDs i llista per stdout la informació de cada un d'aquests grups
# en format: gname: GNAME, gid: GID, users: USERS
# -------------------------------------------------

for arg in $*
do
  grup=$(grep "^[^:]*:[^:]*:$arg:" /etc/group)
  if [ $? -eq 0 ];then
    nom=$(echo "$grup" | cut -d: -f1 | tr 'a-z' 'A-Z')
    users=$(echo "$grup" | cut -d: -f4 | tr 'a-z' 'A-Z')
   echo "gname: $nom gid: $arg users: $users"
  else
   echo "Error: $arg no existeix." >> /dev/stderr
  fi
done
exit 0

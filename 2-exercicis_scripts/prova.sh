
while read -r line
do
	gname=$(grep -E "^[^:]*:[^:]*:$line:" /etc/group | cut -d: -f1)
	users=$(grep -E "^[^:]*:[^:]*:$line:" /etc/group | cut -d: -f4)
	echo "gname: $gname, gid: $line, users:$users"
done
exit 0

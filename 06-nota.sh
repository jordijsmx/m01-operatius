#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Programa que rep 1 nota i diu si està aprovat o no
# -----------------------------------------

# Validar número d'arguments

ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# Validar la nota

if ! [ $1 -ge 0 -a $1 -le 10 ];then
  echo "Error: Nota no vàlida. [0-10]"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# Codi
nota=$1

if [ $nota -lt 5 ];then
  echo "La nota és $nota, suspès."
elif [ $nota -lt 7 ];then
  echo "La teva nota és $nota, aprovat."
elif [ $nota -lt 9 ];then
  echo "La teva nota és $nota, notable."
else
  echo "La teva nota és $nota, excel·lent."
fi
exit 0


function filtrar() {
  total=0
  ok=0
  err=0


  while read -r line
  do
    echo "$line" | grep -w "$1" &> /dev/null
    if [ $? -eq 0 ];then
      echo "$1"
      ((ok++))
    else
      ((err++))
    fi
  ((total++))  
  done
  echo "total: $total, ok: $ok. err: $err"
  return 0
}

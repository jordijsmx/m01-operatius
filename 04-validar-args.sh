#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Programa que rep 2 arguments i els valida
# -----------------------------------------

# Validar número d'arguments
if [ $# -ne 2 ]; then
 echo "Error: número d'arguments incorrecte."
 echo "Usage: $0 arg1 arg2"
 exit 1
fi

# Codi principal
echo "nom: $1"
echo "edat: $2"

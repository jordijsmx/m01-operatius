#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 05-exercici.sh
# opcions: -a -b -c -d
# arguments: resta
# ---------------------------------------

# iterar fent salts
opcions=""
arguments=""
file=""
num=""
while [ -n "$1" ]
do
  case $1 in
    -[abcd])
      opcions="$opcions $1";;
    "-f")
      shift
      file="$1";;
    "-n")
      shift
      num="$1";;
    *)
      arguments="$arguments $1";;
  esac
shift
done
echo -e "Opcions: $opcions \nFile: $file \nNúmero: $num \nArguments: $arguments"
exit 0

# iterar un a un
opcions=""
arguments=""
for arg in $*
do
  case $arg in
    "-a"|"-b"|"-c"|"-d")
      opcions="$opcions $arg";;
    *)
      arguments="$arguments $arg";;
  esac
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0

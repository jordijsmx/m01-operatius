#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 01-exercici.sh
# iterar per llista d'args i realitza una acció
# ---------------------------------------

for arg in $*
do
  accio
  if [ $? -eq 0 ];then
    echo "visca"
  else
    echo "kaka"
  fi
done
exit 0

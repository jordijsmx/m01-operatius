#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 02-exercici.sh
# 
# ---------------------------------------

ok=0
err=0
num=0
status=0

for login in $*
do
  grep "^$login:" /etc/passwd > /dev/null
  if [ $? -eq 0 ];then
    echo "$login"
    ((ok++))
  else
    echo "$login" 1>&2
    ((err++))
    status=1
  fi
  ((num++))
done
echo "total: $num, ok: $ok, error: $err"
exit $status

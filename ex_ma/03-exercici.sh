#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 03-exercici.sh
# rep nom de grup i llista els logins d'arguments
# ---------------------------------------
err=0
grup=$1
shift
for login in $*
do
  sudo usermod -G $grup $login > /dev/null
  if [ $? -ne 0 ];then
    echo "$login" 1>&2
    ((err++))
  fi
done
exit $err

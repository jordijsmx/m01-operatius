#! /bin/bash
# @jordijsmx ASIX-M01
# Març 2023
#
# Rep un o més args, calcula i mostra la suma d'aquests args.
# --------------------

ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 num1..."
  exit $ERR_NARGS
fi

# Codi principal
suma=0

for num in $*
do
  suma=$((num+suma))
done
echo "La suma dels valors introduïts és: $suma"
exit 0

#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# exercici06.sh
# Rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laboralbes i quants festius. Si l'argument
# no és un dia de la setmana genera un error per stderr.
# -----------------------------

ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 dia..."
  exit $ERR_NARGS
fi

# Validar dia de la setmana i codi
laborable=0
festiu=0

for dia in $*
do
  case "$dia" in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
    ((laborable++))
    ;;
    "dissabte"|"diumenge")
    ((festiu++))
    ;;
    *)
    echo "$dia no és un dia de la setmana." >> /dev/stderr
    ;;
  esac
done
echo "Hi ha $laborable dies laborables."
echo "Hi ha $festiu dies festius."
exit 0

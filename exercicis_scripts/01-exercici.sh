#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 01-exercici.sh
# Mostrar el stdin numerant línia a linia
# -----------------------------
num=1

while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

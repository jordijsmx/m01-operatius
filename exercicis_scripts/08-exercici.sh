#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# exercici08.sh
# Rep com a argument noms d'usuari, si existeixen en el sistema
# mostra el nom per stdout. Si no, per stderr.
# -----------------------------

ERR_NARGS=1

# Validar argument
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 nom"
  exit $ERR_NARGS
fi

for user in $*
do
	grep "^$user:" /etc/passwd &> /dev/null # o grep -q (quiet)
  if [ $? -eq 0 ];then
    echo "$user"
  else
    echo "$user" >> /dev/stderr
  fi 
done
exit 0

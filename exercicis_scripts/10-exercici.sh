#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# exercici10.sh
# Rep com a argument un número màxim de línies a mostrar i processa stdin
# línia a línia, numerant el màxim de línies
# -----------------------------

MAX=$1
num=1

while read -r line
do
  if [ $num -le $MAX ];then
    echo "$num: $line"
    ((num++))
  fi
done
exit 0

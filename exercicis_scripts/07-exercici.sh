#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# exercici07.sh
# Processa linia a linia stdin, si la línia té més de
# 60 caràcters la mostra, si no, no.
# -----------------------------

# versio grep
while read -r line
do
  echo "$line" | grep -E "^.{60,}"
done
exit 0

# versio wc
while read -r line
do
  linia=$(echo "$line" | wc -c)
  if [ $linia -gt 60 ];then
  echo "$line"
  fi
done
exit 0

#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 03-exercici.sh
# Fer un comptador des de zero fins al valor indicat per l'argument rebut.
# ---------------------------------------
ERR_NARGS=1

# Validar arguments
if [ $# -ne 1 ];then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 num"
   exit $ERR_NARGS
fi

# Codi
MAX=$1
num=0

while [ $num -le $MAX  ]
do
  echo "$num"
  ((num++))
done
exit 0

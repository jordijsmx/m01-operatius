#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 02-exercici.sh
# Mostrar els arguments rebuts línia a línia, numerant-los.
# ---------------------------------------------

ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 arg1..."
  exit $ERR_NARGS
fi

# Codi
num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0

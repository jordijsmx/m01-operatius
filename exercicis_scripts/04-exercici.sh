#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 04-exercici.sg
# Rep com a arguments números de mes (un o més) i indica per a 
# cada mes rebut quants dies té el mes.
# -----------------------------

ERR_NARGS=1

# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: Número d'arguments incorrecte."
  echo "Usage: $0 mes..."
  exit $ERR_NARGS
fi

# Validació de números de mes i codi
mesos=$*
for mes in $mesos
do
  
  if ! [ $mes -ge 1 -a $mes -le 12 ];then
    echo "Error: Número de mes $mes incorrecte [1-12]"
  else 
  case $mes in
    "2")
    dies=28
    ;;
    "4"|"6"|"9"|"11")
    dies=30
    ;;
    *)
    dies=31
    ;;
esac
  echo "El mes $mes té $dies dies."
fi
done
exit 0

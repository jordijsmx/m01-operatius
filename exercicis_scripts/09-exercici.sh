#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# exercici09.sh
# Rep per stdin noms d'usuari, un per línia, si existeixen en el sistema
# mostra el nom per stdout. Si no, per stderr.
# -----------------------------

while read -r user
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ];then
    echo "$user"
  else
    echo "$user" >> /dev/stderr
  fi 
done
exit 0

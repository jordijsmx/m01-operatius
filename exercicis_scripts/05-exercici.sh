#! /bin/bash

# @jordijsmx ASIX-M01
# Març 2023
# 05-exercici.sh
# Mostra línia a línia stdin, retallant només els primers 50 caràcters.
# -----------------------------

while read -r line
do
  echo "$line" | cut -c 1-50
done
exit 0

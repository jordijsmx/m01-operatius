#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
# 
# Exemple del primer programa
# Normes:
# 	shebang (#!) indica qui interpreta el fitxer
#	capçalera, descripció, data, autor
# --------------------------------------------------
# EXECUTAR UN PROGRAMA:
# bash file
# ./file (permis x) ruta relativa
# source file o . file

echo "hola mon"
nom='pere pou prat'
edat=25
echo -e $nom $edat"\n" # -e per llegir caràcters especials
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uname -a
uptime
ps
echo $SHELL
echo $SHLVL # Al moment d'executar-lo, es crea un bash nou.
echo $((4*32))
echo $((edat*2))

# read data1 data2 # com input de python
# echo -e "$data1 \n $data2" 
# read assigna valors correlativament, si en "sobra" els posa a la última variable
exit 666 # echo $?

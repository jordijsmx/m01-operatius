#! /bin/bash
# @jordijsmx ASIX-M01
# Febrer 2023
#
# Programa que rep 1 argument i valida si és un directori
# -----------------------------------------

ERR_NARGS=1
ERR_DIR=2

# Validar arguments

if [ $# -ne 1 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Validar si és un directori

if [ ! -d $1 ];then
  echo "Error: $1 no és un directori."
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi

# Codi
dir=$1

ls $dir

exit 0
